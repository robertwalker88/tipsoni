from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader

# Create your views here.


def index(request):

    template = loader.get_template('tipsoni/index.html')
    context = {
        'staff': [
            {'id':1,'name':'Gaby'},
            {'id':2,'name':'Luke'},
            {'id':3,'name':'Blaize'},
            {'id':4,'name':'Dan'},
            {'id':5,'name':'Genny'},
        ],
    }
    return HttpResponse(template.render(context, request))
    
