/*
var data = function () {
  return Handsontable.helper.createSpreadsheetData(100, 10);
};

var container = document.getElementById('example');

var hot = new Handsontable(container, {
  data: data(),
  minSpareCols: 1,
  minSpareRows: 1,
  rowHeaders: true,
  colHeaders: true,
  contextMenu: true
});
*/


//$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.click').editable();
    $('#calc').on('click', function(){doFullCalc()});
    $("#weekending").on('hidden',function(e, params){updateDates()});
    $(".staff-row .hours").on('hidden', function(e, reason){
      calcRow($(this).parent().parent());
      if(reason === 'save') {
        //auto-open next editable
        $(this).closest('td').next().find('.click').editable('show');
      }
    });
    $(".tips-row .tips").on('hidden', function(e, reason){
      calcRow($(this).parent().parent());
      if(reason === 'save') {
        //auto-open next editable
        $(this).parent().parent().next().find('td').eq($(this).parent().index()).find('.click').editable('show');
      }
    });
    updateDates();
});

function updateDates(){
  var weekending = moment($("#weekending").text(), "DD/MM/YYYY");
  $('.date-row .date').each(function(){
    var daysBack = parseInt($(this).attr("data-daysback"));
    if(daysBack >= 0){
      $(this).text(weekending.clone().subtract(daysBack, 'day').format('DD MMM YYYY'));
    }
  });

  $('.dayofweek-row .dayofweek').each(function(){
    var daysBack = parseInt($(this).attr("data-daysback"));
    if(daysBack >= 0){
      $(this).text(weekending.clone().subtract(daysBack, 'day').format('dddd'));
    }
  });
}

function calcRows(){
  console.log("Update Calcs");
  $('tr.staff-row,tr.tips-row').each(function(){
    calcRow($(this));
  });
}

function calcRow(row){
    var total=0.0;
    row.find('.hours,.tips').each(function(){
      var v = parseFloat($(this).text());
      if(v){
        total = total+v;
      }
    });
    row.find('.total-row').text(total);
}




function doFullCalc(){
  alert("OK");
  updateDates();

  calcRows();
}
